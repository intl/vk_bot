module.exports =
  {
    'post': function (req, res) {
      if ((!!req.body.bot_id) || (!!req.body.cmd)) {
        if (req.body.cmd != 'log') {
          log('[' + req.body.bot_id + '] Получена комманда: ' + JSON.stringify(req.body.cmd));
        }

        switch (req.body.cmd) {
          case 'get_auth_data':
            {
            if (conf.current >= conf.users.length) {
              conf.current = 0;
              save_config(conf_filename, conf);
            }

            var auth = {'cmd': 'auth_data','bot_id': 'master_mind','args': { 'user': conf.users[conf.current].login,'password': conf.users[conf.current].password}};
            log('[' + req.body.bot_id + '] Будут отправленны данные для аутентификации: ' + JSON.stringify(auth));
            res.status(200).send(auth);
            conf.current++;
            if (conf.current >= conf.users.length)
              conf.current = 0;
            save_config(conf_filename, conf);
            }
            break;

          case 'cmd_request':
            {
            var cmd = {'cmd': conf.cmd,'bot_id': 'master_mind','args': conf.args};
            log('[' + req.body.bot_id + '] Будет отправленна комманда: ' + JSON.stringify(cmd));
            res.status(200).send(cmd);
            }
            break;

          case 'auth_fail':
            {
            var u = req.body.args;

            if ((!!u.user) && (!!u.password)) {
              conf.users.forEach(function auth_fail (item, i, arr) {
                if ((item.login == u.user) && (item.password == u.password)) {
                  if (!!conf.users[i].exclude) {
                    log('[' + req.body.bot_id + '] Авторизация не удалась с логином и паролем: ' + JSON.stringify(u) + ' Логин уже был помечен на исключение, логин будет удален');
                    conf.users.splice(i, 1);
                    if (conf.current > 0)
                      conf.current -= 1;
                  } else {
                    log('[' + req.body.bot_id + '] Авторизация не удалась с логином и паролем: ' + JSON.stringify(u) + ' Логин будет помечен на исключение');
                    conf.users[i].exclude = true;
                  }
                  save_config(conf_filename, conf);
                }
              });
              res.status(200).send('');
            } else {
              log('[' + req.body.bot_id + '] > [' + req.body.cmd + ']: {' + JSON.stringify(req.body.args) + '}');
              res.status(404).send('');
            }
            }
            break;

          case 'auth_ok':
            {
            var u = req.body.args;
            if ((!!u.user) && (!!u.password)) {
              log('[' + req.body.bot_id + '] Авторизация успешна с логином и паролем: ' + JSON.stringify(u));
              conf.users.forEach(function auth_ok (item, i, arr) {
                if ((item.login == u.user) && (item.password == u.password)) {
                  if (!!conf.users[i].exclude) {
                    log('[' + req.body.bot_id + '] Пометка на исключение снята ');
                    conf.users[i].exclude = undefined;
                    save_config(conf_filename, conf);
                  }
                }+ JSON.stringify(u);
              });
            }
            res.status(200).send('');
            }
            break;

          case 'invite_fail':
            if ((typeof req.body.args.group === undefined) || (typeof req.body.args.invite === undefined)) {
              log('[' + req.body.bot_id + '] Ошибка команды: ' + JSON.stringify(req.body.args));
              res.status(200).send('');
              break;
            } else {
              var g = req.body.args.group;
              var i = req.body.args.invite;
              if ((typeof conf.groups[g.name].invites[i.id] === undefined) || (conf.groups[g.name].invites[i.id] == undefined)) {
                log('[' + req.body.bot_id + '] Пользователь не найден что очень странно ');
              } else {
                log('[' + req.body.bot_id + '] Пользователь найден и будет удален из списков на запрет: ');
                conf.groups[g.name].invites[i.id] = undefined;
              }
              res.status(200).send('');
            }
            break;

          case 'send_invite_user_check':

            if ((typeof req.body.args.group === undefined) || (typeof req.body.args.invite === undefined)) {
              log('[' + req.body.bot_id + '] Ошибка команды: ' + JSON.stringify(req.body.args));
              res.status(200).send('');
              break;
            } else {
              /*   {"group":{"join_hash":"0","id":"30009849","name":"invests_in_yourself","type":"GROUP","joined":true},"invite":{"invite_hash":"2b039f6000d24d9184","invite_send":false,"id":"152790914","img_url":"https://pp.vk.me/c323926/v323926914/8836/5LtjHxKQLxo.jpg","id_url":"/id152790914","name":"????? ????????","is_alive":true,"id_school":0}}*/
              log('[' + req.body.bot_id + '] Проверка пользователя на приглашения: ' + JSON.stringify(req.body.args));
              var g = req.body.args.group;
              var i = req.body.args.invite;
              if ((typeof conf.groups === undefined) || (conf.groups == undefined)) {
                conf.groups = {};
                log('[' + req.body.bot_id + '] Groups был пуст и создан новый ' + JSON.stringify(conf.groups));
              }

              if ((typeof conf.groups[g.name] === undefined) || (conf.groups[g.name] == undefined)) {
                log('[' + req.body.bot_id + '] ' + g.name + ' Не найден в conf.groups');
                conf.groups[g.name] = g;
                conf.groups[g.name].invites = {};
                conf.groups[g.name].invites[i.id] = i;
                conf.groups[g.name].invites[i.id].date = moment().format('DD.MM.YY HH:mm:ss');
                save_config(conf_filename, conf);
                send_invite_accept(res, req);
              } else {
                log('[' + req.body.bot_id + '] ' + g.name + ' Найден в conf.groups');
                if ((typeof conf.groups[g.name].invites[i.id] === undefined) || (conf.groups[g.name].invites[i.id] == undefined)) {
                  log('[' + req.body.bot_id + '] ' + i.id + ' Не найден в conf.groups[' + g.name + ']');
                  conf.groups[g.name].invites[i.id] = i;
                  conf.groups[g.name].invites[i.id].date = moment().format('DD.MM.YY HH:mm:ss');
                  save_config(conf_filename, conf);
                  send_invite_accept(res, req);
                } else {
                  log('[' + req.body.bot_id + '] ' + i.id + ' Найден в conf.groups[' + g.name + ']');
                  send_invite_reject(res, req);
                }
              }
            }
            break;

          default:
            log('[' + req.body.bot_id + '] > [' + req.body.cmd + ']: {' + JSON.stringify(req.body.args) + '}');
            res.status(200).send('');
            break;
        }
      } else {
        log('Какие-то неправильные пчелы: ' + JSON.stringify(req.body));
        var cmd = {'error': '404'};
        res.status(404).send(JSON.stringify(cmd));
      }
    },

    'send_invite_reject': function (res, req) {
      var cmd = {'cmd': 'user_not_valid','bot_id': 'master_mind','args': ''};
      log('[' + req.body.bot_id + '] Пользователю уже было отправлено уведомление, отклонение действия: ' + JSON.stringify(cmd));
      res.status(200).send(cmd);
    },

    'send_invite_accept': function (res, req) {
      var cmd = {'cmd': 'user_valid','bot_id': 'master_mind','args': ''};
      log('[' + req.body.bot_id + '] Будет отправленно подтверждение добавления пользователя: ' + JSON.stringify(cmd));
      res.status(200).send(cmd);
    }
};
