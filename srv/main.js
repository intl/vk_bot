var conf_filename = './conf.json';
var log_filename_prefix = './log/';

var express = require('express');
var moment = require('moment');
var bodyParser = require('body-parser');
var conf = read_config(conf_filename);
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.post('/cmd', function post_cmd (req, res) {
  if ((!!req.body.bot_id) || (!!req.body.cmd)) {
    if (req.body.cmd != 'log') {
      log('[' + req.body.bot_id + '] Получена комманда: ' + JSON.stringify(req.body.cmd));
    }

    switch (req.body.cmd) {
      case 'get_auth_data':
        {
        if (conf.current >= conf.users.length) {
          conf.current = 0;
          save_config(conf_filename, conf);
        }
        var auth = {'cmd': 'auth_fail','bot_id': 'master_mind','args': ''};

        for (var i = conf.current; i != conf.users.length; i++) {
          var u = conf.users[i];
          conf.current++;
          if (conf.current >= conf.users.length)
            conf.current = 0;
          save_config(conf_filename, conf);

          if ((!!u.last_used) ? moment(u.last_used).add(1, 'days').isBefore(moment()) : true) {
            log('[' + req.body.bot_id + '] Бот неиспользуемый больше дня найден и будет отправлен');
            var auth = {'cmd': 'auth_data','bot_id': 'master_mind','args': { 'user': u.login,'password': u.password}};
            break;
          }
          else
            log('[' + req.body.bot_id + '] Бот использовался меннее дня, пропуск');
        }

        log('[' + req.body.bot_id + '] Будут отправленны данные для аутентификации: ' + JSON.stringify(auth));
        res.status(200).send(auth);
        }
        break;

      case 'cmd_request':
        {
        var cmd = {'cmd': conf.cmd,'bot_id': 'master_mind','args': conf.args};
        log('[' + req.body.bot_id + '] Будет отправленна комманда: ' + JSON.stringify(cmd));
        res.status(200).send(cmd);
        }
        break;

      case 'auth_fail':
        {
        var u = req.body.args;

        if ((!!u.user) && (!!u.password)) {
          conf.users.forEach(function auth_fail (item, i, arr) {
            if ((item.login == u.user) && (item.password == u.password)) {
              if (!!conf.users[i].exclude) {
                log('[' + req.body.bot_id + '] Авторизация не удалась с логином и паролем: ' + JSON.stringify(u) + ' Логин уже был помечен на исключение, логин будет удален');
                conf.users.splice(i, 1);
                if (conf.current > 0)
                  conf.current -= 1;
              } else {
                log('[' + req.body.bot_id + '] Авторизация не удалась с логином и паролем: ' + JSON.stringify(u) + ' Логин будет помечен на исключение');
                conf.users[i].exclude = true;
              }
              save_config(conf_filename, conf);
            }
          });
          res.status(200).send('');
        } else {
          log('[' + req.body.bot_id + '] > [' + req.body.cmd + ']: {' + JSON.stringify(req.body.args) + '}');
          res.status(404).send('');
        }
        }
        break;

      case 'auth_ok':
        {
        var u = req.body.args;
        if ((!!u.user) && (!!u.password)) {
          log('[' + req.body.bot_id + '] Авторизация успешна с логином и паролем: ' + JSON.stringify(u));
          conf.users.forEach(function auth_ok (item, i, arr) {
            if ((item.login == u.user) && (item.password == u.password)) {
              if (!!conf.users[i].exclude) {
                log('[' + req.body.bot_id + '] Пометка на исключение снята ');
                conf.users[i].exclude = undefined;
              }
              conf.users[i].last_used = moment();
              save_config(conf_filename, conf);
            }
          });
        }
        res.status(200).send('');
        }
        break;

      case 'invite_result':
        if ((typeof req.body.args.group === undefined) || (typeof req.body.args.invite === undefined)) {
          log('[' + req.body.bot_id + '] Ошибка команды: ' + JSON.stringify(req.body.args));
          res.status(200).send('');
          break;
        } else {
          var g = req.body.args.group;
          var i = req.body.args.invite;
          var r = req.body.args.status;
          if ((typeof conf.groups[g.name].invites[i.id] === undefined) || (conf.groups[g.name].invites[i.id] == undefined)) {
            log('[' + req.body.bot_id + '] Пользователь не найден что очень странно ');
          } else {
            log('[' + req.body.bot_id + '] Состояние рассылки: ' + JSON.stringify(r));
            if ((JSON.stringify(r).indexOf('OK') !== -1) || (JSON.stringify(r).indexOf('DENY') !== -1)) {
              log('[' + req.body.bot_id + '] Пользователю установлен статус ' + JSON.stringify(r));
              conf.groups[g.name].invites[i.id].status = r;
            } else {
              log('[' + req.body.bot_id + '] Пользователь найден и будет удален из списков на запрет');
              conf.groups[g.name].invites[i.id] = undefined;
            }
          }
          res.status(200).send('');
        }
        break;

      case 'send_invite_user_check':

        if ((typeof req.body.args.group === undefined) || (typeof req.body.args.invite === undefined)) {
          log('[' + req.body.bot_id + '] Ошибка команды: ' + JSON.stringify(req.body.args));
          res.status(200).send('');
          break;
        } else {
          /*   {"group":{"join_hash":"0","id":"30009849","name":"invests_in_yourself","type":"GROUP","joined":true},"invite":{"invite_hash":"2b039f6000d24d9184","invite_send":false,"id":"152790914","img_url":"https://pp.vk.me/c323926/v323926914/8836/5LtjHxKQLxo.jpg","id_url":"/id152790914","name":"????? ????????","is_alive":true,"id_school":0}}*/
          log('[' + req.body.bot_id + '] Проверка пользователя на приглашения: ' + JSON.stringify(req.body.args));
          var g = req.body.args.group;
          var i = req.body.args.invite;
          if ((typeof conf.groups === undefined) || (conf.groups == undefined)) {
            conf.groups = {};
            log('[' + req.body.bot_id + '] Groups был пуст и создан новый ' + JSON.stringify(conf.groups));
          }

          if ((typeof conf.groups[g.name] === undefined) || (conf.groups[g.name] == undefined)) {
            log('[' + req.body.bot_id + '] ' + g.name + ' Не найден в conf.groups');
            conf.groups[g.name] = g;
            conf.groups[g.name].invites = {};
            conf.groups[g.name].invites[i.id] = i;
            conf.groups[g.name].invites[i.id].date = moment().format('DD.MM.YY HH:mm:ss');
            save_config(conf_filename, conf);
            send_invite_accept(res, req);
          } else {
            log('[' + req.body.bot_id + '] ' + g.name + ' Найден в conf.groups');
            if ((typeof conf.groups[g.name].invites[i.id] === undefined) || (conf.groups[g.name].invites[i.id] == undefined)) {
              log('[' + req.body.bot_id + '] ' + i.id + ' Не найден в conf.groups[' + g.name + ']');
              conf.groups[g.name].invites[i.id] = i;
              conf.groups[g.name].invites[i.id].date = moment().format('DD.MM.YY HH:mm:ss');
              save_config(conf_filename, conf);
              send_invite_accept(res, req);
            } else {
              log('[' + req.body.bot_id + '] ' + i.id + ' Найден в conf.groups[' + g.name + ']');
              send_invite_reject(res, req);
            }
          }
        }
        break;

      default:
        log('[' + req.body.bot_id + '] > [' + req.body.cmd + ']: {' + JSON.stringify(req.body.args) + '}');
        res.status(200).send('');
        break;
    }
  } else {
    log('Какие-то неправильные пчелы: ' + JSON.stringify(req.body));
    var cmd = {'error': '404'};
    res.status(404).send(JSON.stringify(cmd));
  }
});

app.get('/stat', function view_stat (req, res) {
  log(' Просмотр статистики ');
  var o = 'Всего групп в статистике: ' + Object.keys(conf.groups).length + '<hr>';
  for (var g in conf.groups) {
    o += 'В группу ' + conf.groups[g].name + ' было выслано ' + Object.keys(conf.groups[g].invites).length + ' приглашений <br>';
    var ok_cnt = 0;
    var deny_cnt = 0;
    var has_no_status_cnt = 0;
    for (var k in conf.groups[g].invites) {
      if ((typeof conf.groups[g].invites[k].status !== undefined) && (conf.groups[g].invites[k].status !== undefined)) {
        if (!conf.groups[g].invites[k].status.localeCompare('OK')) {
          ok_cnt++;
        }
        if (!conf.groups[g].invites[k].status.localeCompare('DENY')) {
          deny_cnt++;
        }
      } else {
        has_no_status_cnt++;
      }
    }
    o += 'Из них ' + ok_cnt + ' приглашений выслано без ошибок <br>';
    o += deny_cnt + ' пользователей запретили отправлять себе приглашения<br>';
    o += has_no_status_cnt + ' Не отмечены статусом отправки<hr>';
  }

  res.status(200).send(o);
});

app.get('/conf', function view_conf (req, res) {
  log(' Просмотр конфига ');
  res.status(200).send(JSON.stringify(conf));
});

app.get('/log/:date', function view_log (req, res) {
  log(' Просмотр логов за ' + req.params.date);
  fs = require('fs');
  fs.readFile('./log/' + req.params.date, 'utf8', function (err, data) {
    if (err) {
      res.status(404).send('');
    } else {
      res.status(200).send('<pre>' + data + '</pre>');
    }
  });
});

app.get('*', function get_error_404 (req, res) {
  log('Получен запрос: ' + req.path);
  res.status(404).send('<h1>PNH:404</h1>');
});

app.post('*', function post_error_404 (req, res) {
  log('Получен запрос: ' + req.path);
  res.status(404).send('<h1>404</h1>');
});

process.on('uncaughtException', log);

function log (str) {
  if (!!log.caller) {
    str = (moment().format('DD.MM.YY HH:mm:ss')) + ' [' + log.caller.name + '] ' + str;
  } else {
    str = (moment().format('DD.MM.YY HH:mm:ss')) + ' [] ' + str;
  }

  console.log(str);
  var log_filename = log_filename_prefix + (moment().format('DD.MM.YY'));
  fs = require('fs');
  fs.access(log_filename, fs.F_OK, function (err) {
    if (!err) {
      fs.appendFile(log_filename, str + '\r\n', (err) => {
        if (err) throw err;
      });
    } else {
      fs.writeFile(log_filename, str + '\r\n', (err) => {
        if (err) throw err;
      });
    }
  });
}

function save_config (conf_filename, conf) {
  fs = require('fs');
  fs.writeFile(conf_filename, JSON.stringify(conf), function save_conf (err) {
    if (err) {
      return console.log(err);
    }
    log('Файл настроек записан');
  });
}

function read_config (conf_filename) {
  return require(conf_filename);
}

function send_invite_reject (res, req) {
  var cmd = {'cmd': 'user_not_valid','bot_id': 'master_mind','args': ''};
  log('[' + req.body.bot_id + '] Пользователю уже было отправлено уведомление, отклонение действия: ' + JSON.stringify(cmd));
  res.status(200).send(cmd);
}

function send_invite_accept (res, req) {
  var cmd = {'cmd': 'user_valid','bot_id': 'master_mind','args': ''};
  log('[' + req.body.bot_id + '] Будет отправленно подтверждение добавления пользователя: ' + JSON.stringify(cmd));
  res.status(200).send(cmd);
}
app.listen(81);
