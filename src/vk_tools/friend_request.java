/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk_tools;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

/**
 * Класс запросов на дружбу, описывает ответ с ad_friends.php - load_friends_silent
 * Ответ выглядит как-то так: 
 * ['171527286', 'https://pp.vk.me/c630022/v630022286/4a6a4/X6CcdlKLEEk.jpg', '/khorenko88', '2', '0', 'Саша Хоренко', '1', '1', '', ["7c59601363690f2994", "6e0540ea3565df77d2", "https://pp.vk.me/c630022/v630022286/4a6a4/X6CcdlKLEEk.jpg", "", "<div class=\"friends_common_label\"><a class=\"mem_link\" href=\"\/id283941036\">Татьяна Зелинская<\/a> – Ваш общий друг<\/div><div class=\"friends_common_thumbs clear_fix\"><a class=\"friends_common_thumb\" href=\"\/id283941036\" data-title=\"Татьяна Зелинская\" onmouseover=\"showTitle(this);\"><img class=\"friends_common_img\" src=\"https:\/\/pp.vk.me\/c622827\/v622827036\/579a8\/Prxx4s_88og.jpg\"\/><\/a><\/div>", 1, ""]],
 * @author intl
 */
public class friend_request extends friend {
    
    /**
     * Hash для подтверждения
     */
    public String accept_hash;
    /**
     * hash для отклонения дружбы
     */
    public String decline_hash;
    
    @Override
    public String toString() {
        return "ID: '" + this.id 
                + "',Имя: '" + this.name 
                + "', Картинка профиля: '" + this.img_url 
                + "', Ссылка на профиль: '" + this.id_url 
                + "', Активен: " + (is_alive ? "Да" : "Нет")
                + ", Hash для принятия в друзья: "+accept_hash
                + ", Hash для отклонения добавления: "+decline_hash;
    }
}


 class friend_request_deserializer implements JsonDeserializer<friend_request> 
    {
        @Override
        public friend_request deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException 
        {
            friend_request f = new friend_request();
            f.id = json.getAsJsonArray().get(0).getAsString();
            f.img_url = json.getAsJsonArray().get(1).getAsString();
            f.id_url = json.getAsJsonArray().get(2).getAsString();
            f.name = json.getAsJsonArray().get(5).getAsString();
            f.is_alive = (json.getAsJsonArray().get(7).getAsInt()==1);
            f.accept_hash = json.getAsJsonArray().get(9).getAsJsonArray().get(0).getAsString();
            f.decline_hash = json.getAsJsonArray().get(9).getAsJsonArray().get(1).getAsString();
            return f;
        }
    }
