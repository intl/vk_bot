/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk_tools;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import java.util.List;

/**
 *
 * @author intl
 */
public class friend_group_invite extends friend {

    /**
     * Hash приглашения в группу
     */
    public String invite_hash;
    /**
     * Отправлено ли уже приглашение, если true - то да
     */
    public boolean invite_send;

    /**
     * Привести класс профиля к строке
     *
     * @return
     */
    @Override

    public String toString() {
        return "ID: '" + this.id
                + "',Имя: '" + this.name
                + "', Картинка профиля: '" + this.img_url
                + "', Ссылка на профиль: '" + this.id_url
                + "', Активен: " + (is_alive ? "Да" : "Нет")
                + "', Уже получил приглашение ранее: " + (invite_send ? "Да" : "Нет")
                + "', Hash приглашения: " + invite_hash;
    }
}

class friend_group_invite_deserializer implements JsonDeserializer<friend_group_invite> {

    @Override
    public friend_group_invite deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        friend_group_invite f = new friend_group_invite();
        if (json.isJsonArray()) {
            f.id = json.getAsJsonArray().get(0).getAsString();
            f.img_url = json.getAsJsonArray().get(1).getAsString();
            f.id_url = json.getAsJsonArray().get(2).getAsString();
            f.name = json.getAsJsonArray().get(5).getAsString();
            f.is_alive = (json.getAsJsonArray().get(7).getAsInt() == 1);
            f.invite_send = (json.getAsJsonArray().get(11).getAsInt() == 1);
            f.invite_hash = json.getAsJsonArray().get(12).getAsString();
        }
        return f;
    }
}

class friend_group_invite_collection {

    public List<friend_group_invite> all;
}

class friend_group_invite_user_check {

    public Group_t group;
    public friend_group_invite invite;
        
    public friend_group_invite_user_check(Group_t group, friend_group_invite invite) {
        this.group = group;
        this.invite = invite;
    }
}
