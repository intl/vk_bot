/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk_tools;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author intl
 */
public class search_clause_t {

    /**
     * Номер страны 1. - Россия 2. - Украина 3. - Беларусь
     */
    public enum country_t {
        RUSSIA(1), UKRAINE(2), BELARUS(3);
        private int country_num;
        private static final Map<Integer, country_t> map = new HashMap<>();

        static {
            for (country_t e : country_t.values()) {
                map.put(e.country_num, e);
            }
        }

        private country_t(final int c) {
            country_num = c;
        }

        public static country_t valueOf(int c) {
            return map.get(c);
        }
    };
    /**
     * Страна
     */
    public country_t country;
    /**
     * Номер города, список можно получить по:
     * https://vk.com/select_ajax.php?act=a_get_cities&country=2&str=%D0%BA%D0%B0%D1%85%D0%BE%D0%B2%D0%BA%D0%B0
     */
    public int city;
    /**
     * Пол
     * @todo Надо запилить это дело также как country_t
     */
    public enum sex_t {FEMALE,MALE};
    /**
     * Пол
     */
    public sex_t sex;
    /**
     * Номер университета
     */
    public int university;
    /**
     * Номер школы (в VK-щных понятиях)
     */
    public int school;
    /**
     * 1 если с фото, иначе не включается 
     */
    public boolean photo;
    /**
     * Собственно поиск по именни
     */
    public String q;
    /**
     * Возраст от
     */
    public int age_from;
    /**
     * Возраст до
     */
    public int age_to;
    /**
     * Год рождения
     */
    public int byear;
    /**
     * месяц рождения
     */
    public int bmonth;
    /**
     * день рождения
     */
    public int bday;
    /**
     * Компания
     */
    public String company;
}
