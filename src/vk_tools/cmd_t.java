/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk_tools;

import java.util.List;

/**
 *
 * @author intl
 */
public class cmd_t {

    /**
     * Команда боту
     */
    public String cmd;
    /**
     * ID отправителя, используется что бы в сообщениях отсеивать самого себя
     */
    public String bot_id;
    /**
     * Параметры комманды
     */
    public Object args;

    public cmd_t() {
    }

    /**
     * Конструктор
     *
     * @param cmd Команда
     * @param bot_id ID бота
     * @param args Параметры команды
     */
    public cmd_t(String cmd, String bot_id, Object args) {
        this.cmd = cmd;
        this.bot_id = bot_id;
        this.args = args;
    }

    @Override
    public String toString() {
        return cmd;
    }
}
