/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk_tools;

/**
 *
 * @author intl
 */
public class Group_t {
    /**
     * Hash для операции Join
     */
    public String join_hash;
    /**
     * ID группы
     */
    public String id;
    /**
     * Имя группы
     */
    public String name;
    /**
     * Типы групп, группа или паблик
     */
    public enum type_t {GROUP,PUBLIC};
    /**
     * Тип конкретной группы
     */
    public type_t type;
    /**
     * Указывает находится ли текущий пользователь в группе, true - да
     */
    public boolean joined;
    /**
     * Приведение к String
     * @return 
     */
    @Override public String toString()
    {
        return ((type == type_t.GROUP)?"Группа ":"Паблик ")+name+"[ ID: "+id+" ,Hash: "+join_hash+"]"
                +((joined)?" Уже в группе":"Еще не состоим в группе");
    }
}
