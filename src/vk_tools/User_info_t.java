/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk_tools;

import java.util.Objects;

/**
 *
 * @author intl
 */
public class User_info_t {

    /**
     * Имя пользователя/E-mail/номер телефона
     */
    public String user;
    /**
     * Пароль
     */
    public String password;

    User_info_t() {

    }

    /**
     * Приведение к тексту
     *
     * @return
     */
    @Override
    public String toString() {
        return "Логин: " + user + " - " + password;
    }

    /**
     * Функция сравнения обьектов
     * @param other
     * @return
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (!(other instanceof User_info_t)) {
            return false;
        }
        
        User_info_t u = (User_info_t) other;
        return (u.user.equalsIgnoreCase(user)) && (u.password.equals(password));
    }

    /**
     * Функция генерации hash-кода
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.user);
        hash = 17 * hash + Objects.hashCode(this.password);
        return hash;
    }
}
