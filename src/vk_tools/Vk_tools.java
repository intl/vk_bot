/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk_tools;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Arrays;
import java.util.Random;
import org.apache.http.entity.StringEntity;
import org.jsoup.select.Elements;

/**
 *
 * @author intl
 */
public class Vk_tools {

    /**
     * Собственно место ради которого все задумывалось
     */
    public final static String GLOBAL_URL = "https://vk.com/";
    /**
     * Максимальное время задержки между выполнением операций
     */
    public final static int MAX_WAIT_BETWEEN_OPERATIONS = 32768;
    /**
     * URL с которого забирается внешний IP
     */
    public final static String IPIFY_URL = "https://api.ipify.org";
    /**
     * Хост на котором сидит сервер управления
     */
    public final static String MASTER_MIND_HOST = "http://dlab.pw:81/";

    /**
     * Максимальное количество операций (отправленных уведомлений, приглашенных
     * друзей итд за логин)
     */
    public final static int MAX_OPERATIONS_PER_LOGIN = 20;

    /**
     * @value global_headers Глобальные заголовки, а-ля User-Agent итд
     */
    public static Header[] global_headers
            = {
                new BasicHeader("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0"),
                new BasicHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"),
                new BasicHeader("Accept-Encoding", "gzip, deflate, br"),
                new BasicHeader("Accept-Language", "en-US,en;q=0.5"), //new BasicHeader("Connection", "keep-alive");
                new BasicHeader("Refer", GLOBAL_URL),};

    public enum join_group_status_t {
        OK, ERROR, CAPTCHA
    };
    /**
     * Хранилище кукезов
     */
    public static BasicCookieStore global_cookie = new BasicCookieStore();

    /**
     * Выполнения GET запроса к серверу
     *
     * @param url - URL куда будет отправлен запрос, параметры, как известно,
     * передаются в URL
     *
     * @return Ответ в виде HttpResponse
     * @throws IOException
     */
    public static CloseableHttpResponse get_http_response(String url) throws IOException {
        HttpGet req = new HttpGet(url);
        req.setConfig(RequestConfig.custom()
                .setCookieSpec(CookieSpecs.STANDARD)
                .setRedirectsEnabled(true)
                .build());
        URL netUrl = new URL(url);
        String host = netUrl.getHost();
        req.setHeaders(global_headers);
        req.setHeader("Host", host);

        CloseableHttpClient client = HttpClientBuilder.create()
                .setDefaultCookieStore(global_cookie)
                .build();

        CloseableHttpResponse resp = client.execute(req);

        return resp;
    }

    /**
     * Получение значения Location из заголовков ответа
     *
     * @param resp - Ответ c запроса с ответом 302 (Moved)
     * @return - Значение Location из заголовков ответа
     * @throws IOException
     */
    public static String get_location_from_response(CloseableHttpResponse resp) throws IOException {
        String Location = null;
        for (Header h : resp.getAllHeaders()) {
            if (h.getName().equals("Location")) {
                Location = h.getValue();
            }
        }
        return Location;
    }

    /**
     * <p>
     * Отправляет POST запрос</p>
     *
     * @param url - Адрес запроса
     * @param post_data - Данные для запроса
     * @return Ответ в виде CloseableHttpResponse
     * @throws java.io.IOException
     */
    public static CloseableHttpResponse post_http_response(String url, HttpEntity post_data) throws IOException {
        HttpPost req = new HttpPost(url);
        req.setConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build());
        URL netUrl = new URL(url);
        String host = netUrl.getHost();

        req.setHeaders(global_headers);
        req.setHeader("Host", host);
        req.setEntity(post_data);

        CloseableHttpClient client = HttpClientBuilder.create().setDefaultCookieStore(global_cookie).build();
        CloseableHttpResponse resp = client.execute(req);
        return resp;
    }

    /**
     * Аутентификация на VK
     *
     * @param u Параметры пользователя для логина
     * @return true если все хорошо
     */
    public static boolean auth_to_vk(User_info_t u) {
//1. Получить кукезы от VK 
        System.out.println("1. Получение cookie c " + GLOBAL_URL);
        System.out.print("->");

        try (CloseableHttpResponse resp = get_http_response(GLOBAL_URL)) {
            if (resp.getStatusLine().getStatusCode() != 200) {
                if (resp.getStatusLine().getStatusCode() != 302) {
                    System.out.println("Статус: " + resp.getStatusLine());
                    return false;
                } else {
                    //Ооочень спорный нюанс
//                    GLOBAL_URL = get_location_from_response(resp);
                    return false;
                }
            }

            System.out.println("Статус: " + resp.getStatusLine());

            BasicClientCookie remixflash = new BasicClientCookie("remixflash", "11.2.202");
            remixflash.setDomain("vk.com");
            remixflash.setPath("/");
            global_cookie.addCookie(remixflash);

            BasicClientCookie remixscreen_depth = new BasicClientCookie("remixscreen_depth", "24");
            remixscreen_depth.setDomain("vk.com");
            remixscreen_depth.setPath("/");
            global_cookie.addCookie(remixscreen_depth);

            BasicClientCookie remixdt = new BasicClientCookie("remixdt", "0");
            remixdt.setDomain("vk.com");
            remixdt.setPath("/");
            global_cookie.addCookie(remixdt);

            Document vk_com_content = Jsoup.parse(resp.getEntity().getContent(), "UTF-8", GLOBAL_URL);

//2. Обработка страницы vk.com что бы вытянуть данные name="login" id="quick_login_form"
            Element vk_quick_login_form = vk_com_content.select("#quick_login_form").first();
            String vk_login_url = vk_quick_login_form.attr("action");

            List<NameValuePair> vk_post_data = new ArrayList<>();
            vk_quick_login_form.children().stream().filter((e) -> ((e.nodeName().equals("input")) && (!(e.attr("name").equals(""))))).forEach((e) -> {
                vk_post_data.add(new BasicNameValuePair(e.attr("name"), e.attr("value")));
            });

            System.out.println("2. Получение данных с формы: " + vk_login_url);

            vk_post_data.add(new BasicNameValuePair("pass", u.password));
            vk_post_data.add(new BasicNameValuePair("email", u.user));

//2.1 Если в массиве просят ввести капчу..
            /*3.Отправка POST запроса с логином
POSTDATA=act=login&role=al_frame&expire=&captcha_sid=&captcha_key=&_origin=https%3A%2F%2Fvk.com&ip_h=28a2fc5b8b3de6deed&lg_h=4c0e7d292215b52349&email=380983853099&pass=bvmx88%2Flc
             */
            System.out.println("3. Отправка post запроса на авторизацию: ");
            System.out.print("-> ");
            try (CloseableHttpResponse vk_post_resp = post_http_response(vk_login_url, new UrlEncodedFormEntity(vk_post_data))) {
                System.out.println("Статус: " + vk_post_resp.getStatusLine().toString());

                /*
4. Получение Slogin
                 */
//                    global_cookie = get_cookies_from_response(vk_post_resp, global_cookie);
                System.out.println("4. Получение скрипта SLogin с  " + get_location_from_response(vk_post_resp) + " :");
                System.out.print("-> ");

                try (CloseableHttpResponse vk_get_slogin_resp = get_http_response(get_location_from_response(vk_post_resp))) {
                    System.out.println("Статус: " + vk_get_slogin_resp.getStatusLine().toString());
                    //global_cookie = get_cookies_from_response(vk_post_resp, global_cookie);

                    if (global_cookie.getCookies().stream().anyMatch(c -> c.getName().equals("remixsid"))) {
                        System.out.println("5. Залогинилось! ");

                        //8. Проверим на #login_blocked_wrap если есть - страница заблокирована, дальше продолжать нет смысла
                        try (CloseableHttpResponse check_for_block_resp = get_http_response(GLOBAL_URL + "feed")) {
                            if (check_for_block_resp.getStatusLine().getStatusCode() == 200) {
                                Elements login_blocked_wrap = Jsoup.parse(check_for_block_resp.getEntity().getContent(), "UTF-8", GLOBAL_URL).select("#login_blocked_wrap");
                                if (login_blocked_wrap != null) {
                                    System.out.println(login_blocked_wrap.isEmpty() ? "Тесты пройдены, бот жив!" : "Увы, Бота забанили");
                                    return login_blocked_wrap.isEmpty();
                                } else {
                                    return true;
                                }
                            } else {
                                System.out.println("Что-то тут не то.. но пропускаем");
                                //9. Можем начинать рассылку!              
                                return true;
                            }
                        }
                    } else {
                        System.out.println("Авторизация не удалась");
                    }
                }
            } catch (IOException e) {
                System.err.println(e.toString());
            }
        } catch (IOException e) {
            System.err.println(e.toString());
        }
        return false;
    }

    public static void send_message(String Id_to, String Message) throws IOException {
        //1. Для отправки сообщения нужно получить страницу кому нужно написать. vk.com/id... 
        try (CloseableHttpResponse vk_id_to_resp = get_http_response(GLOBAL_URL + Id_to)) {
            //2. В ответе найти profile.init({... 
            //Впринципе это все данные о анкете которой будет отправлена мессага
            //3. В ней найти слово hash: и вырезать все от ' до ' таким образом получится число. Число будет являтся Chas в post
//Кстати hash от страницы к странице не меняется

            String vk_id_to_resp_string = IOUtils.toString(vk_id_to_resp.getEntity().getContent(), "UTF-8");
            int hash_index = vk_id_to_resp_string.indexOf("hash: '") + "hash: '".length();
            if (hash_index > 0) {
                vk_id_to_resp_string = vk_id_to_resp_string.substring(hash_index, hash_index + 18);
                System.out.println("1.Получение hash сообщения; Chas=" + vk_id_to_resp_string);
                /*
      Данные Post:
      act[a_send]
      al[1]
      chas[2449972d822cbb836c]
      from[box]
      media[]
      message[%D0%B2%D1%8B%D1%88%D0%BB%D0%B0] -> { вышла } 
      title[]
      to_ids[272145695]
                 */
                List<NameValuePair> vk_post_data = new ArrayList<>();
                vk_post_data.add(new BasicNameValuePair("act", "a_send"));
                vk_post_data.add(new BasicNameValuePair("al", "1"));
                vk_post_data.add(new BasicNameValuePair("chas", vk_id_to_resp_string));
                vk_post_data.add(new BasicNameValuePair("from", "box"));
                vk_post_data.add(new BasicNameValuePair("media", ""));
                vk_post_data.add(new BasicNameValuePair("message", Message));
                vk_post_data.add(new BasicNameValuePair("Title", ""));
                vk_post_data.add(new BasicNameValuePair("to_ids", Id_to.substring(2)));
                System.out.print("2. Отправка сообщения: ");
                try (CloseableHttpResponse vk_id_post_resp = post_http_response(GLOBAL_URL + "al_mail.php", new UrlEncodedFormEntity(vk_post_data, "UTF-8"))) {
                    System.out.println(vk_id_post_resp.getStatusLine().toString());

                }
            }

        }
    }

    /**
     * Функция получения списка друзей
     *
     * @param id ID профиля у кого надо вытянуть список друзей
     * @return Список друзей в виде Листа с классом friend
     * @throws java.io.IOException
     */
    public static List<friend> get_friends_list(String id) throws IOException {
        /*
        1. Получение списка с ссылки https://vk.com/al_friends.php
        Параметры 'act=load_friends_silent&al=1&gid=0&id=276582820'
         */
        List<friend> returned_friends = new ArrayList<>();
        List<NameValuePair> vk_post_data = new ArrayList<>();
        vk_post_data.add(new BasicNameValuePair("act", "load_friends_silent"));
        vk_post_data.add(new BasicNameValuePair("al", "1"));
        vk_post_data.add(new BasicNameValuePair("gid", "0"));
        vk_post_data.add(new BasicNameValuePair("id", id.substring(2)));

        System.out.print("1. Отправка запроса на получения списка друзей: ");
        try (CloseableHttpResponse friend_list_resp = post_http_response(GLOBAL_URL + "al_friends.php", new UrlEncodedFormEntity(vk_post_data))) {
            System.out.println(friend_list_resp.getStatusLine());
            if (friend_list_resp.getStatusLine().getStatusCode() == 200) {
                /*
                2. Выборка JSON и его разбор    
                 */
                String friend_list_all_obj = Arrays.asList(IOUtils.toString(friend_list_resp.getEntity().getContent(), "UTF-8").split("<!>")).get(5);
                Gson g = new GsonBuilder().registerTypeAdapter(friend.class, new friend_deserializer()).create();
                List<friend> r = (g.fromJson(friend_list_all_obj, friend_collection.class).all);
                if (r != null) {
                    returned_friends.addAll(r);
                }

            }
        }
        System.out.println("2. Загружен список с " + Integer.toString(returned_friends.size()) + " друзьями");
        return returned_friends;
    }

    /**
     * Получение списка запросов на дружбу
     *
     * @param id - Свой id, чужой вернет пустой список
     * @return Список запросов
     * @see friend_request
     * @throws IOException
     */
    public static List<friend_request> get_friend_requests_list(String id) throws IOException {
        /*
        1. Получение списка с ссылки https://vk.com/al_friends.php
        Параметры 'act=load_friends_silent&al=1&gid=0&id=276582820'
         */
        System.out.println("Получение списка запросов на дружбу");
        List<friend_request> returned_friends = new ArrayList<>();
        List<NameValuePair> vk_post_data = new ArrayList<>();
        vk_post_data.add(new BasicNameValuePair("act", "load_friends_silent"));
        vk_post_data.add(new BasicNameValuePair("al", "1"));
        vk_post_data.add(new BasicNameValuePair("gid", "0"));
        vk_post_data.add(new BasicNameValuePair("id", id.substring(2)));

        System.out.print("1. Отправка запроса на получения списка добавления в друзья: ");
        try (CloseableHttpResponse friend_list_resp = post_http_response(GLOBAL_URL + "al_friends.php", new UrlEncodedFormEntity(vk_post_data))) {
            System.out.println(friend_list_resp.getStatusLine());
            if (friend_list_resp.getStatusLine().getStatusCode() == 200) {
                /*
                2. Выборка JSON и его разбор    
                 */
                String friend_list_all_obj = Arrays.asList(IOUtils.toString(friend_list_resp.getEntity().getContent(), get_charset_from_resp(friend_list_resp)).split("<!>")).get(5);
                Gson g = new GsonBuilder().registerTypeAdapter(friend_request.class, new friend_request_deserializer()).create();
                List<friend_request> r = g.fromJson(friend_list_all_obj, friend_request_collection.class).requests;
                if (r != null) {
                    returned_friends.addAll(r);
                }

            }
        }
        System.out.println("2. Загружен список с " + Integer.toString(returned_friends.size()) + " заявками в друзья");
        return returned_friends;
    }

    /**
     * Получение списка возможных друзей
     *
     * @return Список возможных друзей
     * @throws java.io.IOException
     */
    public static List<friend_possible> get_possible_friend_list() throws IOException {
        System.out.println("Получение списка возможных друзей");
        System.out.print("1. Получение https://vk.com/friends?act=find ");
        List<friend_possible> o = new ArrayList<>();
        //1. Получаем get https://vk.com/friends?act=find 
        try (CloseableHttpResponse resp = get_http_response(GLOBAL_URL + "friends?act=find")) {
            System.out.println(resp.getStatusLine());
            if (resp.getStatusLine().getStatusCode() == 200) {
                //2. Ищем тэг <div class="friends_recom_list clear_fix" id="friends_list">
                //3. Выбираем с результата все ".friends_find_user" (Можно в List) 

                //4. В цикле заполняем графы класса: 
                // id = .friends_find_user_add => onclick = Friends.actionFindUser(33528591..
                // img_url = .friends_find_user_img = src
                // id_url = .friends_find_user_name = href
                // name = .friends_find_user_name = innertext
                // is_alive = true;
                // accept_hash = .friends_find_user_add => onclick = Friends.actionFindUser(33528591, 'c9d6b74fb3b0f33de0', true, this);
                // decline_hash = .friends_find_user_remove => onclick = Friends.actionFindUser('33528591', '7f55f49f1ff64a59cd', false, this);
                // from = "possible" 
                Document vk_com_content = Jsoup.parse(resp.getEntity().getContent(), get_charset_from_resp(resp), GLOBAL_URL);
                Elements friend_list = vk_com_content.select("#friends_list");
                if (friend_list != null) {
                    Elements friends_find_user = friend_list.first().select(".friends_find_user");
                    friends_find_user.stream().map((u) -> {
                        friend_possible f = new friend_possible();
                        f.id = substring_from_phrase(u.select(".friends_find_user_add").attr("onclick"), "Friends.actionFindUser(", ");").split(",")[0];
                        f.img_url = u.select(".friends_find_user_img").attr("src");
                        f.id_url = u.select(".friends_find_user_name").attr("href");
                        f.name = u.select(".friends_find_user_name").text();
                        f.is_alive = true;
                        f.accept_hash = substring_from_phrase(u.select(".friends_find_user_add").attr("onclick"), "Friends.actionFindUser(", ");").split(",")[1].replaceAll("[' ]", "");
                        f.decline_hash = substring_from_phrase(u.select(".friends_find_user_remove").attr("onclick"), "Friends.actionFindUser(", ");").split(",")[1].replaceAll("[' ]", "");
                        return f;
                    }).map((f) -> {
                        f.from = "possible";
                        return f;
                    }).forEach((f) -> {
                        o.add(f);
                    });
                }
            }
        }
        System.out.println("2. Получено " + Integer.toString(o.size()) + " анкет");
        return o;
    }

    /**
     * Функция получения списка друзей из поиска по параметрам
     * 
     * @param search - Параметры поиска
     * @see search_clause_t
     * @return
     */
    public static List<friend_request> get_friend_list_by_search(search_clause_t search)
    {   
        /*
1. Получения списка по поиску:
Отправка:  https://vk.com/al_search.php
Параметры: al=1&c[city]=427&c[country]=2&c[sex]=2&c[school]=77224&c[name]=1&c[photo]=1&c[university]=1174403&c[section]=people&change=1&search_loc=friends?act=find&offset=40&c[age_from]=18&c[age_to]=24&c[byear]=1995&c[q]=ирина
        country - опц.
        city - опц, если есть country
        school - опц
        sex - пол, где 1 женский, 2 мужской
        university - Место учебы
        age_from - возраст от
        age_to - возраст до
        byear
        bmonth
        bday
        c[company]=Пирог
        
Отправка: https://vk.com/friends?act=find&c%5Bcity%5D=427&c%5Bcountry%5D=2&c%5Bname%5D=1&c%5Bphoto%5D=1&c%5Bsection%5D=people

Разбор ответа: Ищется тег: div#results
        в этом теге есть (далее ->) div.people_row search_row clear_fix - каждый из которых имеет инфу о аккаунте
            -> div.img ui_zoom_wrap
                -> a.search_item_img_link _online
                    -> <img class="search_item_img" src="https://pp.vk.me/c631518/v631518744/44a29/dMAG4WMw_8E.jpg" alt="Алёна Саламаха">
            -> div.controls
                -> <button id="search_sub100408744" class="flat_button button_small button_wide" onclick="searcher.subscribe(this, 100408744, '30d839276c55de9e71', true, 'search')">Добавить в друзья</button>
            -> div.info
                -> div.labeled name 
                    -> <a href="/id100408744" onclick="return nav.go(this, event);">Алёна Саламаха</a>
        
        
2. Получения списка городов по стране
Отправка: https://vk.com/select_ajax.php
Параметры: act=a_get_country_info&country=2&fields=1
Ответ: {"country":2,"cities":[[427,"<b>Херсон<\/b>"],[314,"<b>Киев<\/b>"],[650,"Днепропетровск (Днепр)"],[223
,"Донецк"],[628,"Запорожье"],[916,"Кривой Рог"],[1057,"Львов"],[552,"Луганск"],[455,"Мариуполь"],[377
,"Николаев"],[292,"Одесса"],[185,"Севастополь"],[627,"Симферополь"],[280,"Харьков"],[761,"Винница"],
[444,"Чернигов"]]}

Отправка (str=Каховка): https://vk.com/select_ajax.php?act=a_get_cities&country=2&str=%D0%BA%D0%B0%D1%85%D0%BE%D0%B2%D0%BA%D0%B0 
Ответ: [['3763','РљР°С…РѕРІРєР°','РљР°С…РѕРІСЃРєРёР№ СЂР°Р№РѕРЅ,<br /> РҐРµСЂСЃРѕРЅСЃРєР°СЏ РѕР±Р»Р°СЃС‚СЊ'
,'1'],['9135','РќРѕРІР°СЏ РљР°С…РѕРІРєР°','РҐРµСЂСЃРѕРЅСЃРєР°СЏ РѕР±Р»Р°СЃС‚СЊ','1']]

3. Получение списка школ начинающихся с строки: 
Отправка: https://vk.com/select_ajax.php?act=a_get_schools&city=427&str=5
Ответ: [[223606,"Гимназия №56"],[40575,"Лицей №58"],[223553,"Профессионально-техническое училище №5"],[445378
,"Таврический лицей искусств №58"],[60914,"Школа №5"],[223592,"Школа №50"],[223600,"Школа №51 им. Г.
 А. Потемкина"],[77224,"Школа №52"],[223607,"Школа №53"],[14462,"Школа №54"],[133482,"Школа №55"],[243924
,"Школа №57"],[126446,"Школа №59 «Хабад»"]]
  
4. Получение списка универов-институтов:
Отправка: https://vk.com/select_ajax.php?act=a_get_universities&country=2&city=427&str=%D1%85
Ответ: [["3088","ХГАУ"],["13215","ХНТУ"],["14657","ХЮИ ХНУВД"],["29921","ХЭПИ"],["77798","МУБиП (ХФ)"],["88136"
,"НУК им. Макарова (бывш. УГМТУ) (ХФ) "],["143504","ГАСУА (ДАСОА) (ХФ)"],["151126","МАУП (ХФ)"],["164744"
,"ХГУ (бывш. ХПИ)"],["459282","КНУКиИ (КНУКиМ) (ХФ)"],["1174403","ХПТК ОНПУ"],["1176140","ХФ ОГУВД"]
]
        
5. Подписка происходит по POST:
        https://vk.com/al_feed.php -> act=subscr&al=1&from=search&hash=30d839276c55de9e71&oid=121874334&ref=friends
        */
        return null;
    }
    
    /**
     * Функция добавления в друзья профиля о ID
     *
     * @param id ID профиля которому будет отправленно уведомление
     * @throws java.io.IOException
     */
    // Добавить ошибки поиска #friend_status
    public static void add_friend(String id) throws IOException {
        //1. Получаем страницу профиля
        System.out.println("Добавление в друзья профиля по ID");

        System.out.print("1.Получение профиля: ");
        try (CloseableHttpResponse resp = get_http_response(GLOBAL_URL + id)) {
            System.out.println(resp.getStatusLine());

            if (resp.getStatusLine().getStatusCode() == 200) {
                //2. Ищем тег  <div class="page_action_left fl_l" id="friend_status">
                System.out.println("2. Поиск тэга friend_status");
                Document vk_com_content = Jsoup.parse(resp.getEntity().getContent(), get_charset_from_resp(resp), GLOBAL_URL);
                String add_friend_btn_onclick = vk_com_content.select("#friend_status").first().html();
                //3. В innerHtml находим Profile.toggleFriend(..
                //4. Вытаскиваем hash операции
                // Возможно переписать не от точки и до +18, а от начала строки, до вхождения в '
                String toggle_friend_str = "Profile.toggleFriend(this, '";
                int hash_from = add_friend_btn_onclick.indexOf(toggle_friend_str);
                if (hash_from != -1) {
                    hash_from += toggle_friend_str.length();
                    int hash_to = add_friend_btn_onclick.indexOf("',", hash_from);
                    String hash = add_friend_btn_onclick.substring(hash_from, hash_to);
                    System.out.println("3. Hash операции: " + hash);
                    //5. Отправляем на https://vk.com/al_friends.php POST с act=add&al=1&from=profile&hash=465782a22eb5830504&mid=383442325
                    System.out.print("4. Отправка запроса на дружбу: ");
                    List<NameValuePair> vk_post_data = new ArrayList<>();
                    vk_post_data.add(new BasicNameValuePair("act", "add"));
                    vk_post_data.add(new BasicNameValuePair("al", "1"));
                    vk_post_data.add(new BasicNameValuePair("from", "profile"));
                    vk_post_data.add(new BasicNameValuePair("mid", id.substring(2)));
                    vk_post_data.add(new BasicNameValuePair("hash", hash));

                    try (CloseableHttpResponse post_resp = post_http_response(GLOBAL_URL + "al_friends.php", new UrlEncodedFormEntity(vk_post_data))) {
                        System.out.println(post_resp.getStatusLine());
                    }
                } else {
                    System.out.println("Кнопка добавить в друзья отсутствует на странице");
                }
            }
        }
    }

    /**
     * Функция добавления в друзья профиля с запроса
     *
     * @param req Запрос на дружбу из get_friend_requests_list
     * @throws java.io.IOException
     */
    // Добавить ошибки поиска #friend_status
    public static void add_friend(friend_request req) throws IOException {

        /*
        1. Отправка POST из того что есть.
        act=add
        al=1
        hash=885c7154a092cb699e
        mid=252948179  //id добавляемого
        request=1
        select_list=1
         */
        System.out.println("1. Добавление в друзья из запросов: id" + req.id);
        List<NameValuePair> vk_post_data = new ArrayList<>();
        vk_post_data.add(new BasicNameValuePair("act", "add"));
        vk_post_data.add(new BasicNameValuePair("al", "1"));
        vk_post_data.add(new BasicNameValuePair("request", "1"));
        vk_post_data.add(new BasicNameValuePair("select_list", "1"));
        vk_post_data.add(new BasicNameValuePair("mid", req.id));
        vk_post_data.add(new BasicNameValuePair("hash", req.accept_hash));
        System.out.print("2. Ответ на запрос: ");
        try (CloseableHttpResponse post_resp = post_http_response(GLOBAL_URL + "al_friends.php", new UrlEncodedFormEntity(vk_post_data))) {
            System.out.println(post_resp.getStatusLine());
            System.out.println("-> " + IOUtils.toString(post_resp.getEntity().getContent(), get_charset_from_resp(post_resp)));
        }

    }

    /**
     * Функция добавления в друзья профиля с возможных знакомы
     *
     * @param req Запрос на дружбу из get_possible_friend_list()
     * @throws java.io.IOException
     */
    public static void add_friend(friend_possible req) throws IOException {

        /*
        1. Отправка POST из того что есть.
        act: "add", {act=add&al=1&from=possible&hash=5da025573be7567c78&mid=74772056}
         */
        System.out.println("1. Добавление в друзья из списка возможных друзей: id" + req.id);
        List<NameValuePair> vk_post_data = new ArrayList<>();
        vk_post_data.add(new BasicNameValuePair("act", "add"));
        vk_post_data.add(new BasicNameValuePair("al", "1"));
        vk_post_data.add(new BasicNameValuePair("from", req.from));
        vk_post_data.add(new BasicNameValuePair("mid", req.id));
        vk_post_data.add(new BasicNameValuePair("hash", req.accept_hash));
        System.out.print("2. Ответ на запрос: ");
        try (CloseableHttpResponse post_resp = post_http_response(GLOBAL_URL + "al_friends.php", new UrlEncodedFormEntity(vk_post_data))) {
            System.out.println(post_resp.getStatusLine());
            System.out.println("-> " + IOUtils.toString(post_resp.getEntity().getContent(), get_charset_from_resp(post_resp)));
        }

    }

    /**
     * Отклонение заявки в друзья
     *
     * @param req Информация о анкете подавшего
     * @throws IOException
     */
    public static void decline_friend(friend_request req) throws IOException {
        /*
        act=remove
al=1
from_section=requests
hash=52a54aa4f1f0e66d01
mid=131977637
report_spam=1
         */
        System.out.println("1. Отклонение добавления в друзья из запросов: id" + req.id);
        List<NameValuePair> vk_post_data = new ArrayList<>();
        vk_post_data.add(new BasicNameValuePair("act", "remove"));
        vk_post_data.add(new BasicNameValuePair("al", "1"));
        vk_post_data.add(new BasicNameValuePair("from_section", "requests"));
        vk_post_data.add(new BasicNameValuePair("report_spam", "1"));
        vk_post_data.add(new BasicNameValuePair("mid", req.id));
        vk_post_data.add(new BasicNameValuePair("hash", req.decline_hash));
        System.out.print("2. Ответ на запрос: ");
        try (CloseableHttpResponse post_resp = post_http_response(GLOBAL_URL + "al_friends.php", new UrlEncodedFormEntity(vk_post_data))) {
            System.out.println(post_resp.getStatusLine());
            System.out.println("-> " + IOUtils.toString(post_resp.getEntity().getContent(), get_charset_from_resp(post_resp)));
        }
    }

    public static join_group_status_t join_group(Group_t g) throws IOException {
        System.out.print("1. Отправка POST запроса на вхождение в группу: ");

        if (g.join_hash.equalsIgnoreCase("0")) {
            System.out.println("Hash на вступление в группу = 0, прерывание");
            return join_group_status_t.ERROR;
        }

        List<NameValuePair> vk_post_data = new ArrayList<>();
        if (g.type == Group_t.type_t.GROUP) {
            vk_post_data.add(new BasicNameValuePair("act", "enter"));
            vk_post_data.add(new BasicNameValuePair("al", "1"));
            vk_post_data.add(new BasicNameValuePair("context", "_"));
            vk_post_data.add(new BasicNameValuePair("gid", g.id));
            vk_post_data.add(new BasicNameValuePair("hash", g.join_hash));
        } else {
            vk_post_data.add(new BasicNameValuePair("act", "a_enter"));
            vk_post_data.add(new BasicNameValuePair("al", "1"));
            vk_post_data.add(new BasicNameValuePair("pid", g.id));
            vk_post_data.add(new BasicNameValuePair("hash", g.join_hash));
        }

        try (CloseableHttpResponse post_resp = post_http_response(GLOBAL_URL + "al_groups.php", new UrlEncodedFormEntity(vk_post_data))) {
            System.out.println(post_resp.getStatusLine());
            String content = IOUtils.toString(post_resp.getEntity().getContent(), get_charset_from_resp(post_resp));
            System.out.println("-> " + content);
            if (content != null) {
                List<String> s = Arrays.asList(content.split("<!"));
                try {
                    switch (Integer.parseInt(s.get(4).replaceAll("[^0-9]+", ""))) {
                        case 0:
                            return join_group_status_t.OK;
                        case 11:
                            return join_group_status_t.CAPTCHA;
                        default:
                            return join_group_status_t.ERROR;

                    }
                } catch (NumberFormatException ex) {
                    System.out.println("Ошибка парсинга ответа: " + ex.toString());
                    return join_group_status_t.ERROR;
                }
            }

        }
        return join_group_status_t.ERROR;
    }

    /**
     * Получить информацию о группе или паблике
     *
     * @param group_name имя группы (К примеру radiointense)
     * @return Параметры группы по имени
     * @throws java.io.IOException
     */
    //Сделать проверку нахожусь ли я в паблике/группе
    // Переделать в две функции, одна нахождение группы, другая для вступления
    public static Group_t group_info(String group_name) throws IOException {
        //1. Получаем страницу группы "vk.com/radiointense"
        System.out.println("Получение информации о группе или паблике");
        Group_t g = new Group_t();
        g.name = group_name;
        System.out.print("1. Получаем страницу группы: ");
        try (CloseableHttpResponse resp = get_http_response(GLOBAL_URL + group_name)) {
            System.out.println(resp.getStatusLine());
            if (resp.getStatusLine().getStatusCode() == 200) {
                //2. Ищем кнопку #join_button если кнопка найдена, тогда переходим к п.3, иначе п.5
                System.out.print("2. Ищем тег #join_button: ");
                Document vk_com_content = Jsoup.parse(resp.getEntity().getContent(), "UTF-8", GLOBAL_URL);
                Elements join_btn = vk_com_content.select("#join_button");

                if (join_btn.size() > 0) {
                    String join_btn_onclick = join_btn.first().attr("onclick");
                    System.out.println("Найден");
                    //3. Забираем значение onclick и забираем от-туда параметры функции, именно hash и номер группы     
                    String find_str = "Groups.enter(this, ";
                    int find_str_index = join_btn_onclick.indexOf(find_str);
                    if (find_str_index != -1) {
                        find_str_index += find_str.length();
                        g.id = join_btn_onclick.substring(find_str_index, join_btn_onclick.indexOf(",", find_str_index + 1));
                        int hash_start_index = join_btn_onclick.indexOf("'", find_str_index) + 1;
                        g.join_hash = join_btn_onclick.substring(hash_start_index, join_btn_onclick.indexOf("'", hash_start_index + 1));
                        g.type = Group_t.type_t.GROUP;
                        g.joined = false;
                        System.out.println("3. Номер группы и hash: " + g.id + " - " + g.join_hash);
                        //4. Отправляем POST запрос на https://vk.com/al_groups.php с параметрами 'act=enter&al=1&context=_&gid=82146824&hash=72d81b6035bbd8bd7e' если все ==200 тогда выходим
                    } else {
                        return null;
                    }

                } else {
                    //5. Если это паблик, и нет кнопки "Вступить" то ищем слово "enterHash":"0db797de1bb7180b73" - это будет hash 
                    System.out.println("Не найден");
                    String page_content = vk_com_content.outerHtml();

                    //6. Ищем класс <div class="page_actions_info"><a onclick="return page.showPageMembers(event, -5500016, 'friends');">Подписаны 2 Ваших друга</a></div> 
                    // Откуда забираем Pid (-5500016), убираем '-' если он есть
                    Elements page_actions_info = vk_com_content.select(".page_actions_info");
                    if (page_actions_info.size() > 0) {
                        System.out.println("Page_actions_info найден, это паблик в который можно войти");
                        System.out.print("Поиск enterHash:");
                        String enter_hash_phrase = "\"enterHash\":\"";
                        int enter_hash_index = page_content.indexOf(enter_hash_phrase);
                        if (enter_hash_index != -1) {
                            System.out.println("Найден");
                            enter_hash_index += enter_hash_phrase.length();

                            g.join_hash = page_content.substring(enter_hash_index, page_content.indexOf("\"", enter_hash_index));
                            g.id = page_actions_info.first().html();
                            String show_page_members_phrase = "return page.showPageMembers(event, ";
                            int group_id_index = g.id.indexOf(show_page_members_phrase);
                            if (group_id_index != -1) {
                                group_id_index += show_page_members_phrase.length();
                                g.id = g.id.substring(group_id_index, g.id.indexOf(",", group_id_index + 1));
                                if (g.id.contains("-")) {
                                    g.id = g.id.substring(1);
                                }
                                g.type = Group_t.type_t.PUBLIC;
                                g.joined = false;
                                System.out.println("3. Номер паблика и hash: " + g.id + " - " + g.join_hash);
                            } else {
                                return null;
                            }
                        } else {
                            System.out.println("Не найден, скорее всего это не группа и не паблик");
                        }
                    } else {
                        System.out.println("Page_actions_info не найден, значит это не группа и не паблик в который можно войти");
                        Elements group_send_msg = vk_com_content.select(".group_send_msg");
                        if (group_send_msg.size() > 0) {
                            g.join_hash = "0";
                            String show_write_msg_box_phrase = "showWriteMessageBox(event, ";
                            g.id = group_send_msg.first().html();
                            int show_write_msg_box_index = g.id.indexOf(show_write_msg_box_phrase);
                            if (show_write_msg_box_index != -1) {
                                show_write_msg_box_index += show_write_msg_box_phrase.length();
                                g.id = g.id.substring(show_write_msg_box_index, g.id.indexOf(")", show_write_msg_box_index + 1));
                                if (g.id.contains("-")) {
                                    g.id = g.id.substring(1);
                                }
                                if (page_content.contains("public.init")) {
                                    g.type = Group_t.type_t.PUBLIC;
                                } else {
                                    g.type = Group_t.type_t.GROUP;
                                }
                                g.joined = true;
                            } else {
                                return null;
                            }
                        } else {
                            System.out.println(".group_send_msg не найден, попробуем через #group_followers или #public_followers");
                            String followers_phrase = "c[group]=";
                            Elements group_followers = vk_com_content.select("#group_followers");
                            if (group_followers.size() > 0) {
                                g.join_hash = "0";
                                g.id = substring_from_phrase(group_followers.first().html(), followers_phrase, "\" ");
                                if (g.id != null) {
                                    g.type = Group_t.type_t.GROUP;
                                    g.joined = true;
                                } else {
                                    return null;
                                }
                            } else {
                                Elements public_followers = vk_com_content.select("#public_followers");
                                if (public_followers.size() > 0) {
                                    g.join_hash = "0";
                                    g.id = substring_from_phrase(public_followers.first().html(), followers_phrase, "\" ");
                                    if (g.id != null) {
                                        g.type = Group_t.type_t.PUBLIC;
                                        g.joined = true;
                                    } else {
                                        return null;
                                    }
                                } else {
                                    g = null;
                                }
                            }
                        }
                    }

                }
            }
        }
        return g;
    }

    /**
     * Получение кодировки текста с resp
     *
     * @param resp Ответ с сайта
     * @return Кодировка
     */
    public static String get_charset_from_resp(CloseableHttpResponse resp) {
        String c = "UTF-8";
        for (Header h : resp.getAllHeaders()) {
            if (h.getName().equalsIgnoreCase("Content-Type")) {
                c = h.getValue().substring(h.getValue().indexOf("charset=") + ("charset=".length()), h.getValue().length());
            }
        }
        return c;
    }

    /**
     * Получение списка запросов на дружбу
     *
     * @param id - Свой id, чужой вернет пустой список
     * @param group_id Id группы для которой будет получен список
     * @return Список запросов
     * @see friend
     * @throws IOException
     */
    public static List<friend_group_invite> get_friend_invite_list(String id, String group_id) throws IOException {
        /*
        1. Получение списка с ссылки https://vk.com/al_friends.php
        Параметры 'act=load_friends_silent&al=1&gid=1234567&id=276582820'
         */
        System.out.println("Получение списка приглашений в группу или паблик");
        List<friend_group_invite> returned_friends = new ArrayList<>();
        List<NameValuePair> vk_post_data = new ArrayList<>();
        vk_post_data.add(new BasicNameValuePair("act", "load_friends_silent"));
        vk_post_data.add(new BasicNameValuePair("al", "1"));
        vk_post_data.add(new BasicNameValuePair("gid", group_id));
        vk_post_data.add(new BasicNameValuePair("id", id.substring(2)));

        System.out.print("1. Отправка запроса на получения списка инвайтов: ");
        try (CloseableHttpResponse friend_list_resp = post_http_response(GLOBAL_URL + "al_friends.php", new UrlEncodedFormEntity(vk_post_data))) {
            System.out.println(friend_list_resp.getStatusLine());
            if (friend_list_resp.getStatusLine().getStatusCode() == 200) {
                /*
                2. Выборка JSON и его разбор    
                 */
                Gson g = new GsonBuilder().registerTypeAdapter(friend_group_invite.class, new friend_group_invite_deserializer()).create();
                //Идеальным случаем была бы проверка всех елементов на JSON-о подобность, и нахождение именно того элемента в котором есть ALL:
                List<friend_group_invite> r = g.fromJson(IOUtils.toString(friend_list_resp.getEntity().getContent(), get_charset_from_resp(friend_list_resp)).split("<!>")[5], friend_group_invite_collection.class).all;
                if (r != null) {
                    returned_friends.addAll(r);
                }

            }
        }
        System.out.println("2. Загружен список с " + Integer.toString(returned_friends.size()) + " друзьями для приглашения в группу");
        return returned_friends;
    }

    /**
     * Отправить приглашение в группу другу из списка инвайтов
     *
     * @param g группа на которую будет отправленно приглашение
     * @param f Друг которому будет отправленно приглашение
     * @return Состояние отправки
     * @throws java.io.IOException
     */
    public static invite_status_t send_invite(Group_t g, friend_group_invite f) throws IOException {
//       curl 'https://vk.com/al_page.php' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: en-US,en;q=0.5' -H 'Connection: keep-alive' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Cookie: remixlang=0; remixflash=11.2.202; remixscreen_depth=24; remixdt=0; remixseenads=0; remixab=1; remixsid=947367c35bab20c8d7fa2ee832da1b1780995728b06cbbe5072ba; remixsslsid=1' -H 'Host: vk.com' -H 'Referer: https://vk.com/friends?act=invite&group_id=4634390' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0' -H 'X-Requested-With: XMLHttpRequest' 
//--data 'act=a_invite&al=1&gid=4634390&hash=8d975a28e4aa543972&mid=272145695'
        //act=a_invite&al=1&gid=82146824&hash=ec760b101075bd0e02&mid=272145695
        invite_status_t res = new invite_status_t(g, f, invite_status_t.status_t.OTHER);
        System.out.println("1. Отправка приглашения в группу");
        List<NameValuePair> vk_post_data = new ArrayList<>();
        vk_post_data.add(new BasicNameValuePair("act", "a_invite"));
        vk_post_data.add(new BasicNameValuePair("al", "1"));
        vk_post_data.add(new BasicNameValuePair("gid", g.id));
        vk_post_data.add(new BasicNameValuePair("hash", f.invite_hash));
        vk_post_data.add(new BasicNameValuePair("mid", f.id));

        try (CloseableHttpResponse resp = post_http_response(GLOBAL_URL + "al_page.php", new UrlEncodedFormEntity(vk_post_data))) {
            System.out.println("Ответ: " + resp.getStatusLine().toString());
            //-> <!--783583353268<!><!>0<!>6744<!>0<!><!int>1<!>����������� �������.
            //-> <!--783583353268<!><!>0<!>6744<!>12<!>268407c6c6d6817e7b
            if (resp.getStatusLine().getStatusCode() == 200) {
                String content = IOUtils.toString(resp.getEntity().getContent(), "UTF-8");
                System.out.println("-> " + content);
                if (content != null) {
                    List<String> s = Arrays.asList(content.split("<!"));
                    try {
                        switch (Integer.parseInt(s.get(s.size() - 2).replaceAll("[^0-9]+", ""))) {
                            case 0:
                                res.status = invite_status_t.status_t.DENY;
                                return res;
                            case 1:
                                res.status = invite_status_t.status_t.OK;
                                return res;
                            case 2:
                            case 12:
                                res.status = invite_status_t.status_t.CAPTCHA;
                                return res;
                            default:
                                res.status = invite_status_t.status_t.OTHER;
                                return res;
                        }
                    } catch (NumberFormatException ex) {
                        System.out.println("Ошибка парсинга ответа: " + ex.toString());
                        res.status = invite_status_t.status_t.OTHER;
                        return res;
                    }
                }
            }

        }
        return res;
    }

    /**
     * Выборка текста между двумя вхождениями Пример: "абвгдежзклмнопрст"
     * вхождение "жзк" выход "прст" - Ответ: "лмно"
     *
     * @param content Входящий текст
     * @param join_phrase Фраза с которой начинается вхождение
     * @param end_phrase Фраза до которой надо обрезать
     * @return Обрезаная фраза
     */
    public static String substring_from_phrase(String content, String join_phrase, String end_phrase) {

        int join_phrase_index = content.indexOf(join_phrase);
        if (join_phrase_index != -1) {
            join_phrase_index += join_phrase.length();
            return content.substring(join_phrase_index, content.indexOf(end_phrase, join_phrase_index + 1));
        } else {
            return null;
        }
    }

    /**
     * Получает свой ID аккаунта бота
     *
     * @return
     * @throws java.io.IOException
     */
    public static String get_my_id() throws IOException {
        //1. Получить vk.com/feed и найти там строку handlePageParams({"id":276582820,...
        //либо "user_id":276582820,...
        //либо "mid":276582820,...
        System.out.println("1. Получение своего VK id");
        String id = null;
        System.out.print("1. Получаем: " + GLOBAL_URL + "feed: ");
        try (CloseableHttpResponse resp = get_http_response(GLOBAL_URL + "feed")) {
            System.out.println(resp.getStatusLine());
            if (resp.getStatusLine().getStatusCode() == 200) {
                String page_content = IOUtils.toString(resp.getEntity().getContent(), "UTF-8");
                id = substring_from_phrase(page_content, "handlePageParams({\"id\":", ",");
                if (id == null) {
                    id = substring_from_phrase(page_content, "\"user_id\":", ",");
                    if (id == null) {
                        id = substring_from_phrase(page_content, "\"mid\":", ",");
                    }
                }
                System.out.println("Свой ID: " + id);
            } else {
                System.out.println("Получить " + GLOBAL_URL + "feed не удалось: " + resp.getStatusLine());
            }
        }
        return (id == null) ? null : "id" + id;
    }

    /**
     * Вызывает ожидание на случайное число до MAX_WAIT_BETWEEN_OPERATIONS
     *
     * @see max_wait_between_operations#MAX_WAIT_BETWEEN_OPERATIONS
     */
    public static void wait_random() {
        try {
            Random thread_wait = new Random();
            int w = thread_wait.nextInt(MAX_WAIT_BETWEEN_OPERATIONS);
            System.out.println("Остановка на: " + Integer.toString(w / 1000) + " секунд");
            Thread.sleep(w);
        } catch (InterruptedException ex) {
            System.err.println(ex);
        }
    }

    /**
     * Получает внешний IP адресс
     *
     * @return внешний IP адресс
     * @throws IOException
     */
    public static String get_my_ip() throws IOException {
        System.out.println("Получение IP бота");
        try (CloseableHttpResponse resp = get_http_response(IPIFY_URL)) {
            if (resp.getStatusLine().getStatusCode() == 200) {
                String ip = IOUtils.toString(resp.getEntity().getContent(), "UTF-8");
                System.out.println("IP: " + ip);
                return ip;
            }
        }
        return null;
    }

    public static void exit(String bot_id) throws IOException {
        System.out.println("Бот [" + bot_id + "] выполнил свою работу и будет завершен");
        StringEntity se = new StringEntity(new Gson().toJson(new cmd_t("disconnect", bot_id, null), cmd_t.class));
        se.setContentType("application/json");
        post_http_response(MASTER_MIND_HOST + "cmd", se);
    }

    public static void server_log(String bot_id, String message) throws IOException {
        System.out.println("Бот [" + bot_id + "]: {" + message + "}");
        StringEntity se = new StringEntity(new Gson().toJson(new cmd_t("log", bot_id, message), cmd_t.class), "UTF-8");
        se.setContentType("application/json");
        post_http_response(MASTER_MIND_HOST + "cmd", se);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            String my_ip = get_my_ip();
            String bot_id = Integer.toHexString(new Random().nextInt(0x7fffffff));
            System.out.println("[" + bot_id + "] Бот запущен, IP: " + my_ip);
            System.out.println("Получение логина и пароля с сервера");
            //1. Забираем с dlab.pw:81 данные для аутентификации
            //{"cmd":"user_used","bot_id":"6c803f6f","args":{"bot":{"user":"380660426393","password":"asdf92/df"},"ip":"46.203.71.122"}}

            StringEntity auth_cmd = new StringEntity(new Gson().toJson(new cmd_t("get_auth_data", bot_id, null)), "UTF-8");
            auth_cmd.setContentType("application/json");

            try (CloseableHttpResponse auth_resp = post_http_response(MASTER_MIND_HOST + "cmd", auth_cmd)) {
                if (auth_resp.getStatusLine().getStatusCode() == 200) {

                    Gson g = new Gson();
                    cmd_t auth_data = g.fromJson(IOUtils.toString(auth_resp.getEntity().getContent(), "UTF-8"), cmd_t.class);
                    if (auth_data.cmd.equalsIgnoreCase("auth_data")) {
                        User_info_t u = g.fromJson(g.toJson(auth_data.args), User_info_t.class);
                        if (auth_to_vk(u)) {
                            server_log(bot_id, "Вход выполнен как пользователь: " + u.toString());

                            cmd_t auth_ok = new cmd_t("auth_ok", bot_id, u);
                            StringEntity cmd_se = new StringEntity(new Gson().toJson(auth_ok, cmd_t.class));
                            cmd_se.setContentType("application/json");
                            post_http_response(MASTER_MIND_HOST + "cmd", cmd_se);

                            wait_random();
                            cmd_t cmd = new cmd_t("cmd_request", bot_id, null);

                            cmd_se = new StringEntity(new Gson().toJson(cmd, cmd_t.class));
                            cmd_se.setContentType("application/json");

                            try (CloseableHttpResponse cmd_resp = post_http_response(MASTER_MIND_HOST + "cmd", cmd_se)) {
                                if (auth_resp.getStatusLine().getStatusCode() == 200) {

                                    cmd_t payload = g.fromJson(IOUtils.toString(cmd_resp.getEntity().getContent(), "UTF-8"), cmd_t.class);
                                    server_log(bot_id, "Получена команда: " + payload.cmd);
                                    /**
                                     * Обработка команды ADD_FRIENDS_INCOMING
                                     */
                                    if ((payload.cmd.equalsIgnoreCase("add_friends_incoming")) && (global_cookie.getCookies().stream().anyMatch(c -> c.getName().equals("remixsid")))) {
                                        String my_id = get_my_id();
                                        server_log(bot_id, "Получена ID текущего пользователя: " + my_id);
                                        wait_random();
                                        List<friend_request> friends = get_friend_requests_list(my_id);
                                        server_log(bot_id, "Получен список из: " + Integer.toString(friends.size()) + " друзей ");
                                        wait_random();
                                        int m = MAX_OPERATIONS_PER_LOGIN;
                                        int pos_cnt = 0, neg_cnt = 0;
                                        for (int i = 0; i != ((friends.size() > m) ? m : friends.size()); i++) {
                                            server_log(bot_id, Integer.toString(i) + ". Добавление в друзья: {" + friends.get(i).toString() + "}");
                                            if (friends.get(i).is_alive) {
                                                add_friend(friends.get(i));
                                                pos_cnt++;
                                                wait_random();
                                            } else {
                                                server_log(bot_id, "Пользователь не активен, заявка отклоняется");
                                                decline_friend(friends.get(i));
                                                neg_cnt++;
                                                wait_random();
                                            }
                                        }
                                        server_log(bot_id, "Было принято " + Integer.toString(pos_cnt) + " заявок, отклонено: " + Integer.toString(neg_cnt));
                                    }

                                    /**
                                     * Обработка команды SEND_INVITES_GROUP
                                     */
                                    if ((payload.cmd.equalsIgnoreCase("send_invites_group")) && (global_cookie.getCookies().stream().anyMatch(c -> c.getName().equals("remixsid")))) {
                                        String my_id = get_my_id();
                                        server_log(bot_id, "Получена ID текущего пользователя: " + my_id);
                                        wait_random();
                                        Group_t gi = group_info(g.fromJson(g.toJson(payload.args), String.class));

                                        if ((gi != null) && (my_id != null)) {
                                            wait_random();
                                            if (!gi.joined) {
                                                server_log(bot_id, "Текущий бот не находится в группе, он будет добавлен");
                                                join_group_status_t j = join_group(gi);
                                                server_log(bot_id, "Статус вхождения в группу: " + j.toString());
                                                gi.joined = (j == join_group_status_t.OK);
                                            }

                                            if (gi.joined) {
                                                server_log(bot_id, "Рассылка приглашений в группу: " + gi.toString());
                                                wait_random();
                                                List<friend_group_invite> invites = get_friend_invite_list(my_id, gi.id);
                                                wait_random();
                                                int m = MAX_OPERATIONS_PER_LOGIN;

                                                for (int i = 0;
                                                        i != ((invites.size() > m) ? m : invites.size()); i++) {
                                                    server_log(bot_id, Integer.toString(i) + ": Отправка приглашения к: " + invites.get(i).toString());
                                                    if (invites.get(i).id != null) {
                                                        cmd_se = new StringEntity(new Gson().toJson(new cmd_t("send_invite_user_check", bot_id, new friend_group_invite_user_check(gi, invites.get(i))), cmd_t.class));
                                                        cmd_se.setContentType("application/json");
                                                        try (CloseableHttpResponse check_user_resp = post_http_response(MASTER_MIND_HOST + "cmd", cmd_se)) {
                                                            if (check_user_resp.getStatusLine().getStatusCode() == 200) {
                                                                payload = g.fromJson(IOUtils.toString(check_user_resp.getEntity().getContent(), "UTF-8"), cmd_t.class);
                                                                if (!payload.cmd.equalsIgnoreCase("user_valid")) {
                                                                    invites.get(i).invite_send = true;
                                                                }
                                                            } else {
                                                                server_log(bot_id, "Сервер не ответил на запрос send_invite_user_check");
                                                                invites.get(i).invite_send = true;
                                                            }
                                                        }

                                                        if (!invites.get(i).invite_send) {
                                                            wait_random();
                                                            invite_status_t res = send_invite(gi, invites.get(i));
                                                            cmd_se = new StringEntity(new Gson().toJson(new cmd_t("invite_result", bot_id, res), cmd_t.class));
                                                            cmd_se.setContentType("application/json");
                                                            post_http_response(MASTER_MIND_HOST + "cmd", cmd_se);
                                                            if (res.status == invite_status_t.status_t.CAPTCHA) {
                                                                server_log(bot_id, "Предлагается ввести капчу, прерываемся");
                                                                break;
                                                            }
                                                        } else {
                                                            server_log(bot_id, "ID" + invites.get(i).id + " уже получил приглашение, пропуск");
                                                            m++;
                                                        }
                                                    } else {
                                                        server_log(bot_id, "Неправильный пользователь в списках: " + invites.get(i).toString());
                                                        m++;
                                                    }
                                                }
                                            } else {
                                                server_log(bot_id, "Боту не удалось войти в группу, рассылка отменена ");
                                            }
                                        }
                                    }

                                    /**
                                     * Обработка команды JOIN_GROUP
                                     */
                                    if ((payload.cmd.equalsIgnoreCase("join_group")) && (global_cookie.getCookies().stream().anyMatch(c -> c.getName().equals("remixsid")))) {
                                        Group_t gi = group_info(g.fromJson(g.toJson(payload.args), String.class));
                                        if (gi != null) {
                                            wait_random();
                                            join_group(gi);
                                            wait_random();
                                        }
                                    }

                                    /**
                                     * Обработка команды ADD_POSIBLE_FRIENDS
                                     */
                                    if ((payload.cmd.equalsIgnoreCase("add_possible_friends")) && (global_cookie.getCookies().stream().anyMatch(c -> c.getName().equals("remixsid")))) {
                                        List<friend_possible> friends = get_possible_friend_list();
                                        server_log(bot_id, "Получен список из " + Integer.toString(friends.size()) + " возможных друзей");
                                        wait_random();

                                        int m = MAX_OPERATIONS_PER_LOGIN;
                                        int pos_cnt = 0;
                                        for (int i = 0; i != ((friends.size() > m) ? m : friends.size()); i++) {
                                            server_log(bot_id, Integer.toString(i) + ". Добавление в друзья: {" + friends.get(i).toString() + "}");
                                            add_friend(friends.get(i));
                                            pos_cnt++;
                                            wait_random();
                                        }
                                        server_log(bot_id, "Было отправлено " + Integer.toString(pos_cnt) + " приглашеиний");

                                    }
                                }
                            }
                        } else {
                            server_log(bot_id, "Войти с данным логином: " + u.toString() + " не удалось");
                            cmd_t auth_fail = new cmd_t("auth_fail", bot_id, u);

                            StringEntity cmd_se = new StringEntity(new Gson().toJson(auth_fail, cmd_t.class));
                            cmd_se.setContentType("application/json");

                            post_http_response(MASTER_MIND_HOST + "cmd", cmd_se);
                        }
                    } else {
                        server_log(bot_id, "Сервер отказал в данных для логина");
                    }
                } else {
                    System.out.println("Получить данные с главного сервера не удалось, выход");
                }
            }
            exit(bot_id);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

}
