/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk_tools;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

/**
 *
 * @author intl
 */
public class friend {

    /**
     * ID друга
     */
    public String id;
    /**
     * URL на заглавное фото
     */
    public String img_url;
    /**
     * URL на страницу
     */
    public String id_url;
    /**
     * Имя друга
     */
    public String name;
    /**
     * Активна ли страница 
     */
    public boolean is_alive;
    /**
     * Номер школы
     */
    public int id_school;
    /**
     * Название работы
     */
    public String work_name;

    /**
     * Пустой конструктор
     */
    public friend() {
    }

    /**
     * Конструктор с заполнением id
     *
     * @param id Номер профиля
     */
    public friend(String id) {
        this.id = id;
    }

    /**
     * Конструктор с заполнением id и фото профилья
     *
     * @param _id Номер профиля
     * @param _img_url Фото профиля
     */
    public friend(String _id, String _img_url) {
        this.id = _id;
        this.img_url = _img_url;
    }

    /**
     * Конструктор с заполнением id и фото профилья
     *
     * @param _id Номер профиля
     * @param _img_url Фото профиля
     * @param _id_url Ссылка на профиль (Номер профиля != Ссылке)
     */
    public friend(String _id, String _img_url, String _id_url) {
        this.id = _id;
        this.img_url = _img_url;
        this.id_url = _id_url;
    }

    /**
     * Конструктор с заполнением id и фото профилья
     *
     * @param _id Номер профиля
     * @param _img_url Фото профиля
     * @param _id_url Ссылка на профиль (Номер профиля != Ссылке)
     * @param _name Имя в профиле
     */
    public friend(String _id, String _img_url, String _id_url, String _name) {
        this.id = _id;
        this.img_url = _img_url;
        this.id_url = _id_url;
        this.name = _name;
    }

    /**
     * Конструктор с заполнением id и фото профилья
     *
     * @param _id Номер профиля
     * @param _img_url Фото профиля
     * @param _id_url Ссылка на профиль (Номер профиля != Ссылке)
     * @param _name Имя в профиле
     * @param _is_alive Если False значит профиль удален
     */
    public friend(String _id, String _img_url, String _id_url, String _name, boolean _is_alive) {
        this.id = _id;
        this.img_url = _img_url;
        this.id_url = _id_url;
        this.name = _name;
        this.is_alive = _is_alive;
    }

    /**
     * Привести класс профилья к строке
     *
     * @return
     */
    @Override
    public String toString() {
        return "ID: '" + this.id
                + "',Имя: '" + this.name
                + "', Картинка профиля: '" + this.img_url
                + "', Ссылка на профиль: '" + this.id_url
                + "', Активен: " + (is_alive ? "Да" : "Нет");
    }
}

class friend_deserializer implements JsonDeserializer<friend> {

    @Override
    public friend deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        friend f = new friend();
        if (json.isJsonArray()) {
            f.id = json.getAsJsonArray().get(0).getAsString();
            f.img_url = json.getAsJsonArray().get(1).getAsString();
            f.id_url = json.getAsJsonArray().get(2).getAsString();
            f.name = json.getAsJsonArray().get(5).getAsString();
            f.is_alive = (json.getAsJsonArray().get(7).getAsInt() == 1);
        }
        return f;
    }
}

