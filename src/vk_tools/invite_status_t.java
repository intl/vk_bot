/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk_tools;

/**
 *
 * @author intl
 */

public class invite_status_t extends friend_group_invite_user_check{

    
    /**
     * Статус отправки приглашения в группы
     */
    public enum status_t {
        DENY, OK, CAPTCHA, OTHER
    };
    
    public status_t status;

    public invite_status_t(Group_t group, friend_group_invite invite,status_t status) {
        super(group, invite);
        this.status = status;
    }
  
}